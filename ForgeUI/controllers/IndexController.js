﻿'use strict';
app.controller('IndexController', ['$scope', 'adalAuthenticationService', '$location', function ($scope, adalAuthenticationService, $location, $rootScope) {
    $scope.location = $location.url();
    $scope.logOut = function () {
        //console.log("adal log out");
        adalAuthenticationService.logOut();
    }

    $scope.LogIn = function () {
        //console.log("adal log IN");
        adalAuthenticationService.login();
    }
    

}]);
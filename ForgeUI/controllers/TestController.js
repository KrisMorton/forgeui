﻿'use strict';
app.controller('TestController', function ($http, $scope) {
    var data = { "ReportID": 1, "Parameters": [{ "ParamName": "MonitoringUnitID", "ParamValue": "15" }, { "ParamName": "ReportProjectID", "ParamValue": "2" }] };


    var request = $http({
        method: "post",
        responseType: 'arraybuffer',
        url: api + "reports",
        data: data
    });

    request.then(function (result) {
        var file = new Blob([result.data], { type: 'application/pdf' });
        var fileURL = URL.createObjectURL(file);
        window.open(fileURL);
    });

});
﻿'use strict';

app.controller('InspectorTimeOffController', function ($scope, $modal, $modalInstance, $location, UserServices, Inspector, Alerts, ScheduleTypeService, InspectorScheduleService) {
    $scope.currentUser = UserServices.GetCurrentUser();
  
    $scope.selectedDate = new Date();
    $scope.inspector = Inspector;
    $scope.isCollapsed = true;
    $scope.selectedSchedule = { ID: undefined };
    $scope.inspectorSchedules = undefined;
    $scope.scheduleTypes = null;
    $scope.filterDate = new Date();


    $scope.LoadSchedules = function (date) {
        var promise = InspectorScheduleService.GetByInspectorDate($scope.inspector.ID,date.toDateString());
        promise.then(function (result) {
            $scope.inspectorSchedules = result.data;

        },
     function (errorResult) {
         Alerts.push({ type: 'danger', msg: "Failure loading Inspector Schedules" })
     });
    }



    $scope.LoadScheduleTypes = function () {
        var promise = ScheduleTypeService.GetServiceTypes();
            promise.then(function (result) {
                $scope.scheduleTypes = result.data;
         
            },
         function (errorResult) {
             Alerts.push({ type: 'danger', msg: "Failure loading schedule types" })
         });
    };

    $scope.SaveSchedule = function () {

        $scope.selectedSchedule.InspectorID = $scope.inspector.ID;
        if ($scope.selectedSchedule.ID === undefined)
        {
            var promise = InspectorScheduleService.Post($scope.selectedSchedule);
            promise.then(function (result) {
                Alerts.push({ type: 'success', msg: "Schedule Saved" });
                $scope.LoadSchedules($scope.selectedSchedule.ScheduleDate);
                $scope.selectedSchedule = { ID: undefined };
                $scope.isCollapsed = true;
               
               
            },
             function (errorResult) {
                 console.log(errorResult);
                 Alerts.push({ type: 'danger', msg: errorResult.data.Message });
             });

        }
        else
        {
            var promise = InspectorScheduleService.Put($scope.selectedSchedule);
            promise.then(function (result) {
                Alerts.push({ type: 'success', msg: "Schedule Saved" });
                $scope.LoadSchedules($scope.filterDate);
                $scope.selectedSchedule = { ID: undefined };
                $scope.isCollapsed = true;

            },
             function (errorResult) {
                 console.log(errorResult);
                 Alerts.push({ type: 'danger', msg: errorResult.data.Message });
             });
        }

    };
    $scope.EditSchedule = function (inspectorSchedule) {
      
        var startd = new Date(inspectorSchedule.EstimatedStartTime);
        var endD = new Date(inspectorSchedule.EstimatedEndTime);

        angular.extend($scope.selectedSchedule, inspectorSchedule);

        $scope.selectedSchedule.EstimatedStartTime = startd;
        $scope.selectedSchedule.EstimatedEndTime = endD;

        $scope.isCollapsed = false;

    };
    $scope.DeleteSchedule = function(inspectorSchedule){
        var promise = InspectorScheduleService.Delete(inspectorSchedule);
        promise.then(function (result) {
            Alerts.push({ type: 'success', msg: "Schedule Deleted" });
            $scope.LoadSchedules($scope.filterDate);
            $scope.selectedSchedule = { ID: undefined };
            $scope.isCollapsed = true;
               
        },
         function (errorResult) {
             console.log(errorResult);
             Alerts.push({ type: 'danger', msg: errorResult.data.Message });
         });
    };


    $scope.ReloadSchedules = function(){
        $scope.LoadSchedules($scope.filterDate);
    };


    //DatePickerAdd
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 0
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'M/d/yyyy'];
    $scope.format = $scope.formats[3];


    ////DatePicker Filter
    $scope.showWeeks = false;


    $scope.openFilter = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.openedFilter = true;
    };

    $scope.dateOptionsFilter = {
        'year-format': "'yy'",
        'starting-day': 1,
        'datepicker-mode':"'day'",
        'min-mode':"day"
    };

    $scope.LoadScheduleTypes();
    $scope.LoadSchedules( $scope.filterDate);

    $scope.closeModal = function () {
        $modalInstance.close();
    };
});

﻿

'use strict';

app.controller('RecurringDispatchController', function ($scope, $modal,$modalInstance, $location, UserServices, Alerts, StartDate, Dispatch) {
    $scope.currentUser = UserServices.GetCurrentUser();
    $scope.keepInspector = false;
    $scope.includeSaturday = false;
    $scope.includeSunday = false;
    $scope.startDate = StartDate;
    $scope.dispatch = Dispatch;

    $scope.DuplicateDispatch = {DispatchID: null, BeginDate: null, EndDate: null, IncludeSaturday: false, IncludeSunday: false, KeepAssignedInspector: false, IsRecurring: true };


    $scope.CreateRecurring = function () {

     //   $scope.DuplicateDispatch.DispatchID = $scope.dispatch.id;
        $scope.closeModal();
    };




    //DatePickerSettings
    $scope.clear = function () {
        $scope.endDate = null;
    };
    $scope.toggleMin = function () {
        $scope.minDate = $scope.startDate ? null : new Date();
    };
    $scope.toggleMin();
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'M/d/yyyy'];
    $scope.format = $scope.formats[0];




    $scope.closeModal = function () {
        $modalInstance.close($scope.DuplicateDispatch)
    }
});
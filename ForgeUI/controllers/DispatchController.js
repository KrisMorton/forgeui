﻿'use strict';

app.controller('DispatchController', function ($scope, $modal, $position,  $location, UserServices, DispatchService, _) {
    $scope.currentUser = UserServices.GetCurrentUser();
    $scope.showAllBranches = false;
    $scope.dispatches = undefined;
    $scope.alerts = [];
    $scope.radioModel = 'TodayOnly';
    $scope.groupedDispatches = [];
    //$scope.selectedDate = new Date();
    $scope.selectedDispatch = undefined;
    $scope.keepInspector = false;
    $scope.duplicatePopover = {
        templateUrl: 'duplicatedispatch.html',
        selectedDate: new Date(),
        keepInspector: false
    };

    $scope.SetSelectedDispatch = function (dispatch) {
        $scope.selectedDispatch = dispatch;
      
    };

    $scope.CopyDispatch = function () {
        $scope.popoverIsOpen = false;

        $scope.duplicateDispatch = {
            DispatchID: $scope.selectedDispatch.DispatchID,
            BeginDate: $scope.duplicatePopover.selectedDate,
            EndDate: $scope.duplicatePopover.selectedDate,
            IncludeSaturday: false,
            IncludeSunday: false,
            KeepAssignedInspector: $scope.duplicatePopover.keepInspector,
            IsRecurring: false
        };

        var promise = DispatchService.PostRecurring($scope.duplicateDispatch);
        promise.then(function (result) {
            $scope.selectedDispatch.popoverIsOpen = false;
            $scope.alerts.push({ type: 'success', msg: "Dispatch Copied" })
            $scope.LoadDispatches();
        },
       function (errorResult) {
           $scope.alerts.push({ type: 'danger', msg: "Failure Copying Dispatch" })
       });

    };

    $scope.ClosePopover = function () {
        $scope.selectedDispatch.popoverIsOpen = false;
    };
    


    $scope.LoadDispatches = function () {
        var utcOffset = moment().utcOffset();
        console.log("offset =" + utcOffset);
        var promise = DispatchService.GetDispatches(!$scope.showAllBranches, $scope.radioModel, utcOffset);
        promise.then(function (result) {
            $scope.dispatches = result.data;
            $scope.groupedDispatches = _.groupBy($scope.dispatches, function (value) {
                var d = new Date(value.DispatchDate);
                return d.toLocaleDateString()
            });
        },
      function (errorResult) {
          $scope.alerts.push({ type: 'danger', msg: "Failure Loading Dispatches" })
      });
    };

    $scope.SetColor = function (dispatch) {
        if (dispatch.AssignedInspector === null)
        {
            return { 'background-color': "red" }
        }
    };

    $scope.go = function (path) {
       
        $location.path(path);
    };

    /*Modal Functions*/
    $scope.ContactInfo = function (dispatch) {
        var modalInstance = $modal.open({
            templateUrl: 'dispatchcontact.html',
            controller: 'DispatchContactController',
            resolve: {
                Dispatch: function () {
                    return dispatch;
                }

            }
        });

    };

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    /*Date Picker*/
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'M/d/yyyy'];
    $scope.format = $scope.formats[3];
    $scope.LoadDispatches();

});
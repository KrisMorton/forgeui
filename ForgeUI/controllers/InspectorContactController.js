﻿'use strict';

app.controller('InspectorContactController', function ($scope, $modal, $modalInstance, $location, UserServices, Inspector, ContactInfoType) {
    $scope.currentUser = UserServices.GetCurrentUser();
    $scope.inspector = Inspector;


    $scope.closeModal = function () {
        $modalInstance.close();
    }
});

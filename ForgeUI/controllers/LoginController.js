﻿'use strict';


app.controller('LoginController', function ($scope, $rootScope, $location, UserServices) {
    $scope.userInfo = $rootScope.userInfo;
     $scope.Login = function (user) {
        var promise = UserServices.GetForgeUser($scope.userInfo.userName);
        promise.then(function (result) {
            UserServices.SetCurrentUser(result.data);
            $scope.ForgeUser = result.data;
            $location.path("/Dispatch").replace();
          
            
        });
    }
    $scope.Login();
});
'use strict';

app.controller('InspectorCertificationsController', function ($scope, $modal, $modalInstance, $location, UserServices, CertificationTypeService, CertificationService,InspectorService, Alerts,Inspector) {
  
    $scope.currentUser = UserServices.GetCurrentUser();
    $scope.inspector = Inspector;
    $scope.selectedCertification = { ID: undefined };
    $scope.certificationTypes = null;
    $scope.selectedCertificationType = null;
    $scope.isCollapsed = true;


    //Loading Funcitons
    $scope.LoadCertificationTypes = function () {
          var promise = CertificationTypeService.GetCertificationTypes();
             promise.then(function (result) {
                $scope.certificationTypes = result.data;

            },
         function (errorResult) {
             $scope.alerts.push({ type: 'danger', msg: "Failure loading Certification Types" })
         });
    };
    $scope.LoadInspector = function (id) {
        var promise = InspectorService.Get(id);
        promise.then(function (result) {
            $scope.inspector = result.data;
            $scope.isCollapsed = true;

        },
      function (errorResult) {
          $scope.alerts.push({ type: 'danger', msg: "Failure Reloading Inspector" })
      });
    };


    $scope.EditCertification = function (certification) {
        angular.extend($scope.selectedCertification, certification);
        var formattedDate = new Date($scope.selectedCertification.ExpirationDate);
        $scope.selectedCertification.ExpirationDate = formattedDate;

        $scope.isCollapsed = false;

    };


    //Save Function
    $scope.SaveCertification = function () {
        $scope.selectedCertification.InspectorID = $scope.inspector.ID;
        if ($scope.selectedCertification.ID === undefined)
        {
            console.log('POST');
            var promise = CertificationService.Post($scope.selectedCertification);
            promise.then(function (result) {
                $scope.selectedCertification = { id: undefined };

                Alerts.push({ type: 'success', msg: "Certification Saved" });

                $scope.LoadInspector($scope.inspector.ID);

            },
            function (errorResult) {
                Alerts.push({ type: 'danger', msg: "Failure Saving  Certification" })
            });

        }
        else
        {
            console.log('PUT');
            var promise = CertificationService.Put($scope.selectedCertification);
            promise.then(function (result) {
                $scope.selectedCertification = { id: undefined };

                $scope.LoadInspector($scope.inspector.ID);

            },
            function (errorResult) {
                Alerts.push({ type: 'danger', msg: "Failure Saving  Certification" })
            });

        }
    };

    //DatePicker
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'M/d/yyyy'];
    $scope.format = $scope.formats[0];



    $scope.LoadCertificationTypes();
    $scope.closeModal = function () {
        $modalInstance.close();
    };

});

﻿'use strict';

app.controller('UpsertDispatchController', function ($scope, $modal,$location, UserServices, DispatchService, DispatchQuestionService, ProjectServices, InspectionTypeService, InspectionTypeQuestionService, ContactService, ContactInfoType, ContactInfoService, Dispatch) {
    $scope.currentUser = UserServices.GetCurrentUser();
    var d = new Date();
    //$scope.dt = d.toLocaleDateString();
    $scope.selectedProject = undefined;
    $scope.alerts = [];
    $scope.Dispatch = Dispatch;
    $scope.selectedInspectionType = undefined;
    $scope.inspectiontypes = undefined;
    $scope.inspectionquestions = undefined;
    $scope.useProjectAddress = false;
    $scope.vm = {
        address: {}
    };
    $scope.hstep = 1;
    $scope.mstep = 15;
    $scope.mytime = new Date();
    $scope.isRecurring = undefined;
    $scope.ismeridian = true;
    $scope.contacts = undefined;
    $scope.selectedContact = undefined;
    $scope.showContactInfo = false;
    $scope.missingContact = {};
    $scope.selectedContactInfoType = {};
    $scope.selectedContactInfoValue = undefined;
    $scope.selectedContactPrimary = false;
    $scope.missingContactInfos = [];
    $scope.receivedTypes = ["Phone", "Email", "Other"];
    $scope.selectedReceivedType = undefined;
    $scope.SaveNewContactID = null;
    $scope.duplicateDispatch = null
    $scope.buttonLabel = 'Recurring Event';
    $scope.IsEdit = false;
    $scope.headerText = "New Dispatch"

    //DropDown and Other Collections
    $scope.LoadContacts = function (val) {
        if (val.length > 2)
        {
            var promise = ContactService.GetContactSearch(val);
            return promise.then(
                function (response) {
                    return response.data;
                });
        }
    };
    $scope.LoadContactInfoTypes = function () {
        var promise = ContactInfoType.GetContactTypes();
        promise.then(function (result) {
            $scope.contactInfoTypes = result.data;
           
        },
     function (errorResult) {
         $scope.alerts.push({ type: 'danger', msg: "Failure loading Contact Info Types" })
     });

    };
    $scope.LoadInspectionTypes = function () {
        var promise = InspectionTypeService.GetAllTypes();
        promise.then(function (result) {
            $scope.inspectiontypes = result.data;
        },
         function (errorResult) {
             $scope.alerts.push({ type: 'danger', msg: "Failure loading Inspection Types" })
         });

    };
    $scope.LoadInspectionQuestions = function (inspectiontype) {
        var promise = InspectionTypeQuestionService.GetNewQuestions(inspectiontype.ID);
        promise.then(function (result) {
            $scope.inspectionquestions = result.data;
        },
        function (errorResult) {
            $scope.alerts.push({ type: 'danger', msg: "Failure loading Inspection Types Questions" })
        });

    }
    $scope.LoadProjects = function () {
        var promise = ProjectServices.LoadAllProjects();
        promise.then(function (result) {
            $scope.projects = result.data;
        },
         function (errorResult) {
             $scope.alerts.push({ type: 'danger', msg: "Failure loading Projects" })
         });

    };

    //Add
    $scope.AddProject = function () {
        var modalInstance = $modal.open({
            templateUrl: 'addproject.html',
            controller: 'AddProjectController',
            resolve: {
                Project: function () {
                    return $scope.selectedProject;
                },
                Alerts: function () {
                    return $scope.alerts;
                }

            }
        });
        modalInstance.result.then(function (Project) {
            $scope.selectedProject = Project;
        })

    };

    //Recurring Dispatch
    $scope.RecurringDispatch = function () {
        console.log($scope.isRecurring);
        if ($scope.isRecurring === true)
        {
            $scope.buttonLabel = 'Cancel Recurring';
            var modalInstance = $modal.open({
                templateUrl: 'recurringdispatch.html',
                controller: 'RecurringDispatchController',
                size: 'lg',
                resolve: {
                    Alerts: function () {
                        return $scope.alerts;
                    },
                    StartDate: function () {
                        return $scope.mytime;
                    },
                    Dispatch: function () {
                        return $scope.Dispatch;
                    }

                }
            });
            modalInstance.result.then(function (duplicateDispatch) {
                console.log(duplicateDispatch);
                $scope.duplicateDispatch = duplicateDispatch;
                if ($scope.duplicateDispatch.EndDate === null)
                {
                    $scope.isRecurring = false;
                    $scope.RecurringDispatch();
                }

            })
        }
        else
        {
            $scope.buttonLabel = 'Recurring Event';

        }

    };


    $scope.AddContactInfo = function () {
        $scope.missingContactInfos.push({
            contactInfoTypeID: $scope.selectedContactInfoType.ID,
            description: $scope.selectedContactInfoType.Description,
            contactID: null,
            infoValue: $scope.selectedContactInfoValue,
            primaryContact: $scope.selectedContactPrimary

        });
        // Clear input fields after push
        $scope.selectedContactInfoType = "";
        $scope.selectedContactInfoValue = "";
        $scope.selectedContactPrimary = false;
    };
    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };
    $scope.CancelDispatch = function () {
        $location.path("/Dispatch");
    };

    //Value Setters
    $scope.SetDispatchProjectAddress = function () {
        if ($scope.useProjectAddress)
        {
            $scope.Dispatch.Address = $scope.selectedProject.Address;
            $scope.Dispatch.City = $scope.selectedProject.City;
            $scope.Dispatch.State = $scope.selectedProject.State;
            $scope.Dispatch.ZipCode = $scope.selectedProject.ZipCode;
        }
        else
        {
            $scope.Dispatch.Address = null;
            $scope.Dispatch.City = null;
            $scope.Dispatch.State = null;
            $scope.Dispatch.ZipCode = null;
            $scope.Dispatch.Latitude = null;
            $scope.Dispatch.Longitude = null;
        }
    };
    $scope.SetDispatchAddress = function (UseProjectAddress) {
        if (!UseProjectAddress)
        {
            if ($scope.vm.address.street_number == undefined)
            {
                $scope.Dispatch.Address = $scope.vm.address.formattedAddress;

            }
            else
            {
                $scope.Dispatch.Address = $scope.vm.address.street_number + ' ' + $scope.vm.address.route;
                $scope.Dispatch.City = $scope.vm.address.locality;
                $scope.Dispatch.State = $scope.vm.address.administrative_area_level_1;
                $scope.Dispatch.ZipCode = $scope.vm.address.postal_code;
                $scope.Dispatch.Latitude = $scope.vm.address.lat;
                $scope.Dispatch.Longitude = $scope.vm.address.lng;
                $scope.vm.address.formattedAddress = undefined
            }
        }
    };

    //Save Functions
    $scope.SaveNewContact = function () {
        if ($scope.showContactInfo)
        {
            if ($scope.selectedContactInfoType !== "" && $scope.selectedContactInfoValue !== "")
                $scope.AddContactInfo();

            var promise = ContactService.Post($scope.missingContact);
            promise.then(function (result) {
                $scope.selectedContact = result.data;
                for (var i = 0, l = $scope.missingContactInfos.length; i < l; i++)
                {
                    var missingContactInfo = $scope.missingContactInfos[i];
                    missingContactInfo.contactID = $scope.selectedContact.ID
                    var promise2 = ContactInfoService.Post(missingContactInfo);
                    promise2.then(function (result) {
                      
                    },
                    function (errorResult) {
                        $scope.alerts.push({ type: 'danger', msg: "Failure Saving New Contact Info" })
                    });

                }

                $scope.SaveDispatch();
            },
            function (errorResult) {
                $scope.alerts.push({ type: 'danger', msg: "Failure Saving New  Contact" })
            });
        }
        else
            $scope.SaveDispatch();

    };
    $scope.SaveDispatch = function () {
        var ret = true;
        var dispatchdatestring = $scope.mytime.toGMTString();
        $scope.Dispatch.DispatchDate = new Date(dispatchdatestring);
        $scope.Dispatch.ProjectID = $scope.selectedProject.ID;
        $scope.Dispatch.contactID = $scope.selectedContact.ID;
        $scope.Dispatch.inspectionTypeID = $scope.selectedInspectionType.ID;
        $scope.Dispatch.dispatchStatusID = 1;
        $scope.Dispatch.receivedBy = $scope.currentUser.l_ForgeUserDTO.UserName;
        $scope.Dispatch.receivedDate = new Date();

        if ($scope.Dispatch.receivedByMethod === "")
            $scope.Dispatch.receivedByMethod = $scope.selectedReceivedType;
      
        $scope.Dispatch.StartTimeText = $scope.mytime.toLocaleTimeString(('en-US'), { hour: '2-digit', minute: '2-digit' });

       
        var promise3 = DispatchService.Post($scope.Dispatch);
        promise3.then(function (result) {
            $scope.Dispatch = result.data;
          
            var d = new Date($scope.Dispatch.DispatchDate);
            for (var i = 0, l = $scope.inspectionquestions.length; i < l; i++)
            {
                var inspectionquestion = $scope.inspectionquestions[i];

                var dispatchQuestion = {};
                dispatchQuestion.dispatchID = $scope.Dispatch.ID;
                dispatchQuestion.inspectionTypeQuestionID = inspectionquestion.ID;
                dispatchQuestion.response = inspectionquestion.Response;
                var promise4 = DispatchQuestionService.Post(dispatchQuestion);
                promise4.then(function (result) {
                   
                },
                function (errorResult) {
                    $scope.alerts.push({ type: 'danger', msg: "Failure Saving Dispatch Question" })
                });

            }
            if ($scope.isRecurring === true)
            {
                $scope.duplicateDispatch.DispatchID = $scope.Dispatch.ID;
                $scope.duplicateDispatch.BeginDate = moment(new Date($scope.Dispatch.DispatchDate)).add(1, 'd');
                console.log($scope.duplicateDispatch.BeginDate);

                var promise5 = DispatchService.PostRecurring($scope.duplicateDispatch);
                promise5.then(function (result) {

                },
               function (errorResult) {
                   $scope.alerts.push({ type: 'danger', msg: "Failure Creating Recurring Dispatch" })
               });




            }
          
            $location.path("/Dispatch");
        },
       function (errorResult) {
           $scope.alerts.push({ type: 'danger', msg: "Failure Dispatch" })
       });

    };
    $scope.SaveQuestion = function (question) {
            $scope.dispatchQuestion = {
            ID: question.ID,
            DispatchID: $scope.Dispatch.DispatchID,
            InspectionTypeQuestionID: question.InspectionTypeQuestionID,
            Response: question.QuestionResponse
            };

            var promise = DispatchQuestionService.Put($scope.dispatchQuestion);
            promise.then(function (result) {
                $scope.alerts.push({ type: 'success', msg: "Response Updated" })
                question.CanEdit = false;

            },
            function (errorResult) {
                $scope.alerts.push({ type: 'danger', msg: "Failure Updating Response" })
            });


    };
    $scope.UpdateDispatch = function () {


        var dispatchdatestring = $scope.mytime.toGMTString();

        var updateDispatch = {
            ID: $scope.Dispatch.DispatchID,
            ProjectID: $scope.Dispatch.ProjectID, 
            ContactID: $scope.selectedContact.ID,
            InspectionTypeID: $scope.Dispatch.InspectionTypeID,
            DispatchStatusID: $scope.Dispatch.DispatchStatusID,
            DispatchDate: new Date(dispatchdatestring),
            Duration: $scope.Dispatch.Duration,
            Address: $scope.Dispatch.Address,
            Address2: $scope.Dispatch.Address2,
            City: $scope.Dispatch.City,
            State: $scope.Dispatch.State,
            ZipCode: $scope.Dispatch.ZipCode,
            Latitude: $scope.Dispatch.Latitude,
            Longitude: $scope.Dispatch.Longitude,
            ReceivedBy: $scope.Dispatch.ReceivedBy,
            ReceivedDate: $scope.Dispatch.ReceivedDate,
            ReceivedByMethod: $scope.Dispatch.ReceivedByMethod,
            StartTimeText: $scope.mytime.toLocaleTimeString(('en-US'), { hour: '2-digit', minute: '2-digit' }),
            Note: $scope.Dispatch.Note
        }

        var promise = DispatchService.Put(updateDispatch);
        promise.then(function (result) {
            $location.path("/Dispatch");
        },
         function (errorResult) {
             $scope.alerts.push({ type: 'danger', msg: "Failure Updating Dispatch" })
         });

    };


    //Value Reset Functions
    $scope.clearInfoValue = function () {
        $scope.selectedContactInfoValue = undefined;
    };
    $scope.clearReceivedMethod = function () {
        $scope.Dispatch.receivedByMethod = "";
    }
    $scope.clearSelectedContact = function () {
        $scope.selectedContact = undefined;

    };

    //DatePickerSettings
    $scope.clear = function () {
        $scope.dt = null;
    };
    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'M/d/yyyy'];
    $scope.format = $scope.formats[0];

    //Initial Load
    $scope.LoadProjects();
    $scope.LoadInspectionTypes();
    $scope.LoadContactInfoTypes();
    $scope.LoadEditValues = function () {
        $scope.mytime = $scope.Dispatch.DispatchDate;
        $scope.selectedContact = $scope.Dispatch.DispatchContact;
        $scope.headerText = "Edit Dispatch";
    };


    if ($scope.Dispatch !== null)
    {
        $scope.IsEdit = true;
        $scope.LoadEditValues();

    }

});
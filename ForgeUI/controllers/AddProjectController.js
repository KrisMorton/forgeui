﻿'use strict';

app.controller('AddProjectController', function ($scope, $modal,$modalInstance, UserServices, ProjectServices, Alerts,Project) {
    $scope.Project = {};
    $scope.Project.JobNumber = "Not Available";
    $scope.currentUser = UserServices.GetCurrentUser();
  
    $scope.vm = {
        address: {}
    };
 
    $scope.UpdateProject = function () {
        if ($scope.vm.address.street_number == undefined)
        {
            $scope.Project.Address = $scope.vm.address.formattedAddress;
            $scope.Project.City = null;
            $scope.Project.State = null;
            $scope.Project.ZipCode = null;
            $scope.Project.Latitude = null;
            $scope.Project.Longitude = null;
            $scope.Project.BranchID = $scope.currentUser.l_ForgeUserDTO.BranchID;
        }
        else
        {
            $scope.Project.Address = $scope.vm.address.street_number + ' ' + $scope.vm.address.route;
            $scope.Project.City = $scope.vm.address.locality;
            $scope.Project.State = $scope.vm.address.administrative_area_level_1;
            $scope.Project.ZipCode = $scope.vm.address.postal_code;
            $scope.Project.Latitude = $scope.vm.address.lat;
            $scope.Project.Longitude = $scope.vm.address.lng;
            $scope.Project.BranchID = $scope.currentUser.l_ForgeUserDTO.BranchID;
        }

    }
    
    $scope.Add = function (Project) {
        var promise = ProjectServices.Post(Project);
        promise.then(function (result) {
            $scope.Project = result.data;

            $scope.closeModal();
            Alerts.push({ type: 'success', msg: "Temporary Project Added" });
        },
        function (errorResult) {
            Alerts.push({ type: 'danger', msg: "Error Adding Project" });
        });

    }


    $scope.closeModal = function () {
        $modalInstance.close($scope.Project);
    }
})
﻿'use strict';

app.controller('InspectorController', function ($scope, $modal, $location, UserServices, ProjectServices, InspectorService) {
    $scope.alerts = [];
    $scope.currentUser = UserServices.GetCurrentUser();
    $scope.inspectors = null;
    $scope.displayAllInspectors = false;
    $scope.search = "";


    //Loading Functions
    $scope.LoadTechsByBranch = function (id) {
        var promise = InspectorService.GetInspectorsByBranch(id);
        promise.then(function (result) {
            $scope.inspectors = result.data;
         
        },
        function (errorResult) {
            $scope.alerts.push({ type: 'danger', msg: "Failure loading inspectors" })
        });
    };


    $scope.GetAllInpectors = function () {
        if ($scope.displayAllInspectors)
        {
            var promise = InspectorService.GetInspectors();
            promise.then(function (result) {
                $scope.inspectors = result.data;

            },
            function (errorResult) {
                $scope.alerts.push({ type: 'danger', msg: "Failure loading inspectors" })
            });
        }
        else
        {

            $scope.LoadTechsByBranch($scope.currentUser.l_ForgeUserDTO.BranchID);
        }

    };


    //Modal Functions
    $scope.ContactInfo = function (inspector) {
        var modalInstance = $modal.open({
            templateUrl: 'inspectorcontact.html',
            controller: 'InspectorContactController',
            resolve: {
                Inspector: function () {
                    return inspector;
                }
               
            }
        });
       
    };
    $scope.Certifications = function (inspector) {
        var modalInstance = $modal.open({
            templateUrl: 'inspectorcertifications.html',
            controller: 'InspectorCertificationsController',
            resolve: {
                Inspector: function () {
                    return inspector;
                },
                Alerts: function () {
                    return $scope.alerts;
                }

            }
        });
        modalInstance.result.then(function () {
            $scope.LoadTechsByBranch($scope.currentUser.l_ForgeUserDTO.BranchID);
        })
    };

    $scope.ScheduleVacation = function (inspector) {
        var modalInstance = $modal.open({
            templateUrl: 'inspectortimeoff.html',
            controller: 'InspectorTimeOffController',
            size: 'lg',
            resolve: {
                Inspector: function () {
                    return inspector;
                },
                Alerts: function () {
                    return $scope.alerts;
                }
            }
        });

    };


    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };
    $scope.LoadTechsByBranch($scope.currentUser.l_ForgeUserDTO.BranchID);
});
﻿

'use strict';

app.controller('DispatchContactController', function ($scope, $modal, $modalInstance, $location, UserServices, Dispatch, ContactInfoType) {
    $scope.currentUser = UserServices.GetCurrentUser();
    $scope.dispatch = Dispatch;


    $scope.closeModal = function () {
        $modalInstance.close();
    }
});

﻿
'use strict';

app.service('ContactService', function ($http) {
    return {
        GetContactSearch: function (Search) {
            console.log(Search);
            return $http.get(api + "Contacts/"+Search);
        }, 

        GetAllContacts: function () {
            return $http.get(api + "Contacts");
        },

        Get: function (ID) {
            return $http.get(api + "Contacts/" + ID);

        },
        Post: function (Contact) {
            var data = JSON.stringify(Contact);
            var request = $http({
                method: "post",
                url: api + "Contacts",
                data: data
            });
            return request;
        }

    }
});
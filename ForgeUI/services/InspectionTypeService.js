﻿
'use strict';

app.service('InspectionTypeService', function ($http) {
    return {
        GetAllTypes: function(){
            return $http.get(api + "InspectionTypes");
        },

        Get: function (ID) {
            return $http.get(api + "InspectionTypes/" + ID);

        }
    }
});
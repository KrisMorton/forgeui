﻿'use strict';
app.service('AlertServices', function () {
    var alerts = [];
    
    this.AddAlert = function (message, type) {
        alerts.push({ type: type, msg: message });
    }

    this.CloseAlert = function (index) {
        alerts.splice(index, 1);
    }

    this.ClearAlerts = function () {
        alerts = [];
    }

    this.GetAlerts = function () {
        return alerts;
    }
});
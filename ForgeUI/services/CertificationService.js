﻿
'use strict';

app.service('CertificationService', function ($http) {
    return {
        Get: function (ID) {
            return $http.get(api + "<>/" + ID);

        },
        Post: function (Certification) {
            var data = JSON.stringify(Certification);
            console.log(Certification);
            var request = $http({
                method: "post",
                url: api + "Certifications",
                data: data
            });
            return request;
        },
        Put: function (Certification) {
            var data = JSON.stringify(Certification);
            var request = $http({
                method: "put",
                url: api + "Certifications/" + Certification.ID,
                data: data
            });
            return request;
        }

    }
});

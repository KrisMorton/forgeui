﻿'use strict';
app.service('ProjectServices', function ($http, StorageService) {
    StorageService.Set();
    return {
        LoadAllProjects: function() {
            return $http.get(api + "Projects");
        },
        Post: function (Project) {
            var data = JSON.stringify(Project);
            console.log(Project);
            var request = $http({
                method: "post",
                url: api + "Projects",
                data: data
            });
            return request;
        },
        Get: function (ID) {
            return $http.get(api + "Projects/" + ID);

        },
        Delete: function (Projects) {
            var request = $http({
                method: "delete",
                url: api + "Projects/" + Projects.ID,
            });
            return request;
        },
        Put: function (Projects) {
            var data = JSON.stringify(Projects);
            var request = $http({
                method: "put",
                url: api + "Projects/" + Projects.ID,
                data: data
            });
            return request;
        }
    }
});
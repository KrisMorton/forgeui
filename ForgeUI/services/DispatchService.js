﻿
'use strict';
app.service('DispatchService', function ($http, UserServices, StorageService) {
    StorageService.Set();
    return {
        GetDispatches: function (bybranch, range, utcoffset ) {
            var currentUser = UserServices.GetCurrentUser();
            console.log(currentUser);
            return $http.get(api + "Branch/Dispatches/" + currentUser.l_ForgeUserDTO.BranchID + "/" + bybranch + "/" + range + "/" + utcoffset);
        },

        GetAllDispatches: function (date) {
            return $http.get(api + "/Dispatches/" + date);
        },
        GetUnAssignedDispatches: function (branchID) {
            return $http.get(api + "Branch/Unassigned/Dispatches/" + branchID);
            },
        Post: function (Dispatch) {
            var data = JSON.stringify(Dispatch);
            var request = $http({
                method: "post",
                url: api + "Dispatches",
                data: data
            });
            return request;
        },
        PostRecurring: function (DuplicateDispatch) {
            var data = JSON.stringify(DuplicateDispatch);
            var request = $http({
                method: "post",
                url: api + "DuplicateDispatch",
                data: data
            });
            return request;
        },
        Get: function (ID) {
            return $http.get(api + "Dispatch/" + ID );

        },
        Delete: function (Dispatch) {
            var request = $http({
                method: "delete",
                url: api + "Dispatches/" + Dispatch.ID,
            });
            return request;
        },
        Put: function (Dispatch) {
            var data = JSON.stringify(Dispatch);
            var request = $http({
                method: "put",
                url: api + "Dispatches/" + Dispatch.ID,
                data: data
            });
            return request;
        },


    }
});
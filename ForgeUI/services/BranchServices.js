﻿'use strict';

app.service('BranchServices', function ($http, $cookieStore) {
    var currentBranch = null;
    return {
        SetCurrentBranch: function (branchID) {
            currentBranch = client;
            $cookieStore.put('currentBranch', JSON.stringify(currentBranch));
            sessionStorage.currentBranch = JSON.stringify(client);
        },
        GetCurrentBranch: function () {
            return currentBranch;
        }

    }
});
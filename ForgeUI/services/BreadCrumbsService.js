﻿app.factory('BreadCrumbsService', function () {
    var breadcrumbs = []
    if (sessionStorage.breadcrumbs != null || sessionStorage.breadcrumbs != '' || sessionStorage.breadcrumbs != undefined) {
        
        try {
            var breadcrumbs = JSON.parse(sessionStorage.breadcrumbs);
        } catch (e) {
            
        }
    } 
    
    return {
        push: function (label, href) {
            var breadcrumb = {
                label: label,
                href: href
            }


            //breadcrumbs.push(breadcrumb);
            breadcrumbs.push(breadcrumb);
            sessionStorage.breadcrumbs = JSON.stringify(breadcrumbs);

        },
        reset: function (label) {
            var temp = [];
            for (var bc in breadcrumbs) {

                temp.push(breadcrumbs[bc]);
                
                if (breadcrumbs[bc].label === label) {
                    breadcrumbs = temp;
                   
                    break;
                }
            }
            sessionStorage.breadcrumbs = JSON.stringify(breadcrumbs);

        },
        get: function () {
            return breadcrumbs;
        },
        pop: function (index) {
            breadcrumbs.splice(index, 1);
        },
        clear: function () {
            breadcrumbs = [];
            sessionStorage.breadcrumbs = JSON.stringify(breadcrumbs);
        }


    }
});
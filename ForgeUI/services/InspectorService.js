﻿
'use strict';
app.service('InspectorService', function ($http) {
      return {
        GetInspectorsByBranch: function (id) {
            return $http.get(api + "Branch/Inspector/" + id);
        },
        GetInspectors: function () {
            return $http.get(api + "/Inspectors");
        },
        Get: function (ID) {
            return $http.get(api + "Inspectors/" + ID);

       }
    }
});
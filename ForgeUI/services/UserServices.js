﻿'use strict';
app.service("UserServices", function ($http, $location) {
    var currentUser = null;

    return {
        GetForgeUser: function (userName) {
            return $http.get(api + "ForgeUser/?userName=" + userName);
        },

        SetCurrentUser: function (user) {
            currentUser = user;
            sessionStorage.currentUser = JSON.stringify(user);
            //$cookies.currentUser = user;
            
         
        },
        GetCurrentUser: function () {
            if (currentUser != null || currentUser != undefined) {
                return currentUser;
            } else {
                try {
                    return JSON.parse(sessionStorage.currentUser);
                } catch (ex) {
                    console.log("User not in local storage");
                    console.log("User not logged in sending to login")
                    $location.url('/');
                }
            }
        }
    }
});
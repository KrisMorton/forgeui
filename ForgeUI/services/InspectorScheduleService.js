﻿
'use strict';

app.service('InspectorScheduleService', function ($http) {
    return {
        
        GetByInspectorDate: function (ID, date) {
            console.log(date);
              return $http.get(api + "Inspector/InspectorSchedules/" + ID + "/" + date);
        },

        Get: function (ID) {
            return $http.get(api + "<>/" + ID);

        },
        Post: function (InspectorSchedule) {
            var data = JSON.stringify(InspectorSchedule);
            var request = $http({
                method: "post",
                url: api + "InspectorSchedules/",
                data: data
            });
            return request;
        },
        Put: function (InspectorSchedule) {
            var data = JSON.stringify(InspectorSchedule);
            var request = $http({
                method: "put",
                url: api + "InspectorSchedules/" + InspectorSchedule.ID,
                data: data
            });
            return request;
        },
        Delete: function (InspectorSchedule) {
            var request = $http({
                method: "delete",
                url: api + "InspectorSchedules/" + InspectorSchedule.ID,
            });
            return request;
        }




    }
});



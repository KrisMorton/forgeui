﻿


'use strict';
app.service('DispatchQuestionService', function ($http) {
      return {
       
        Post: function (DispatchQuestion) {
            var data = JSON.stringify(DispatchQuestion);
            var request = $http({
                method: "post",
                url: api + "DispatchQuestions",
                data: data
            });
            return request;
        },
        Put: function (DispatchQuestion) {
            var data = JSON.stringify(DispatchQuestion);
            var request = $http({
                method: "put",
                url: api + "DispatchQuestions/" + DispatchQuestion.ID,
                data: data
            });
            return request;
        }
    }
});
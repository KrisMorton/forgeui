'use strict';

/**
 * @ngdoc overview
 * @name ngCrumbsApp
 * @description
 * # ngCrumbsApp
 *
 * Main module of the application.
 */

var app = angular.module('ngCrumbs', []);


app.factory('BreadCrumbsService', function(){

  var breadcrumbs = [{label: "Home", href: "#/"}, {label: "Foo", href: "#/"}];
  return {
    push: function(lable, href){
      var breadcrumb ={
        label: label,
        href: href
      }
      breadcrumbs.push(breadcrumb);
    },
    reset: function( index ){
      var temp = [];
      for (var i = 0; i <= index; i++) {
        temp.push(breadcrumbs[i]);
      }
      breadcrumbs = temp;
    },
    get: function(){
      return breadcrumbs;
    }

  }
});

app.directive('crumbs', function( BreadCrumbsService ){
  return {
    restrict: 'E',
    templateUrl: 'views/template.html',
    controller: function($scope, BreadCrumbsService){

      $scope.breadcrumbs = BreadCrumbsService.get();

      $scope.reset = function(index){
        BreadCrumbsService.reset(index);
        $scope.breadcrumbs = [];
        $scope.breadcrumbs = BreadCrumbsService.get();
      }
    },
  }


});

'use strict';

/**
 * @ngdoc function
 * @name ngCrumbsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ngCrumbsApp
 */
angular.module('ngCrumbsApp')
  .controller('MainCtrl', function ($scope, BreadCrumbsService) {
    $scope.breadcrumbs = BreadCrumbsService.get();
    console.log("BreadCrumbs");
    $scope.reset = function(index){
      BreadCrumbsService.reset(index);
      $scope.breadcrumbs = [];
      $scope.breadcrumbs = BreadCrumbsService.get();
    }
  });

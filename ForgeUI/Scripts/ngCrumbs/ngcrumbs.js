'use strict';

/**
 * @ngdoc overview
 * @name ngCrumbsApp
 * @description
 * # ngCrumbsApp
 *
 * Main module of the application.
 */

var app = angular.module('ngCrumbs', []);


app.factory('BreadCrumbsService', function( $rootScope){
  
    $rootScope.breadcrumbs = [{ label: "Projects", href: '#/' }];
  return {
    push: function(label, href){
      var breadcrumb ={
        label: label,
        href: href
      }
      
      
      //breadcrumbs.push(breadcrumb);
      $rootScope.breadcrumbs.push(breadcrumb);
      
    },
    reset: function( label ){
      var temp = [];
      for (var bc in $rootScope.breadcrumbs) {
          
          temp.push($rootScope.breadcrumbs[bc]);
          
          if ($rootScope.breadcrumbs[bc].label === label) {
              
              $rootScope.breadcrumbs = temp;
              
              break;
          }
      }
     
      
    },
    get: function(){
      return breadcrumbs;
    },
    pop: function ( index ) {
        breadcrumbs.splice(index, 1);
    },


  }
});

app.directive('crumbs', function( BreadCrumbsService ){
  return {
    restrict: 'E',
    template: "<ul class=\"breadcrumb\">" +
                "<li ng-repeat=\"bc in breadcrumbs\" ng-class=\"{true: 'active'}[$last]\">" +
                    "<span ng-if=\"!$last\"><a ng-click=\"reset(bc.label)\" ng-href=\"{{bc.href}}\">{{bc.label}}</a></span>" +
                    "<span ng-if=\"$last\">{{bc.label}}</span>" +
                "</li></ul>",
    controller: function($scope, $rootScope, BreadCrumbsService){
      
        $scope.breadcrumbs = $rootScope.breadcrumbs;
      
      $scope.reset = function(label){
        BreadCrumbsService.reset(label);
        $scope.breadcrumbs = [];
        $scope.breadcrumbs = BreadCrumbsService.get();
        
      }
    },
  }


});

﻿'use strict';
app.filter('ProjectFilter', function ($filter) {
    return function (list, input) {
        //If no input given return whole list
        if (!input) return list;
        //Return items matching ProjectName first, if there are none then it will check JobNumbers.
        var result = $filter('filter')(list, { 'ProjectName': input });
        if (result.length <= 0) result = $filter('filter')(list, { 'JobNumbers': input });
        return result;
    }
})
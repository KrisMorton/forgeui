﻿

app.directive('localtimeformat', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelController) {
            ngModelController.$formatters.push(function (value) {
                var d = new Date(value);
                return d.toLocaleTimeString()
            });
        }
    };
});


app.directive('localdateformat', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelController) {
            ngModelController.$formatters.push(function (value) {
                var d = new Date(value);
                return d.toLocaleDateString()
            });
        }
    };
});


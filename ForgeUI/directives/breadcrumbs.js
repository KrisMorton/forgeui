﻿app.directive('crumbs', function (BreadCrumbsService) {
    return {
        restrict: 'E',
        template: "<ul class=\"breadcrumb\">" +
                    "<li ng-repeat=\"bc in breadcrumbs track by $index\" ng-class=\"{true: 'active'}[$last]\">" +
                        "<span ng-if=\"!$last\"><a ng-click=\"reset(bc.label)\" ng-href=\"{{bc.href}}\">{{bc.label}}</a></span>" +
                        "<span ng-if=\"$last\">{{bc.label}}</span>" +
                    "</li></ul>",
        controller: function ($scope, BreadCrumbsService) {

            $scope.breadcrumbs = BreadCrumbsService.get();
            
            $scope.reset = function (label) {
                BreadCrumbsService.reset(label);
                $scope.breadcrumbs = [];
                $scope.breadcrumbs = BreadCrumbsService.get();
                
            }
        },
    }


});
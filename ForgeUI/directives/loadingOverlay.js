﻿/// <reference path="../views/overlay.html" />

app.directive('loading', function ($http) {
    $('.overlay').affix();
    return {
        restrict: 'A',
        templateUrl: '../views/overlay.html',
        link: function(scope, elm, attrs){
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };

            scope.$watch(scope.isLoading, function (v) {
                if (v) {
                    elm.show();
                } else {
                    elm.hide();
                }
            });
        }
    }
})
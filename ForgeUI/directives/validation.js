﻿var DATE_REGEXP = /([0-1][0-2])?([0]?[0-9])?\/([3][0-1])?([0]?[1-9])?([1-2]?[0-9])?\/\d{4}/;
app.directive("date", function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$validators.date = function (modelValue, viewValue) {
                console.log(viewValue);
                //value from datepicker is a Date object. This converts it to string so the regex could be ran for validation purposes.
                if (viewValue instanceof Date) {
                     try {
                        date = new Date(viewValue);
                        viewValue = date.getDay() + "/" + date.getMonth() + "/" + date.getFullYear();
                    } catch (e) {
                        console.log("A valid date was not entered");
                    }
                }
                if (ctrl.$isEmpty(modelValue)) {
                    console.log("invalid");
                    return false;
                }
                
                if (DATE_REGEXP.test(viewValue)) {
                    console.log("valid");
                    return true;
                } 
                return false;
            }
        }
    }
});

app.directive("dropdown", function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$validators.date = function (modelValue, viewValue) {

                if (ctrl.$isEmpty(modelValue)) {
                    console.log("invalid");
                    return false;
                }

                if (modelValue != '') {
                    console.log("valid");
                    return true;
                }
                return false;
            }
        }
    }
});

app.directive("text", function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$validators.date = function (modelValue, viewValue) {

                if (ctrl.$isEmpty(modelValue)) {
                    return false;
                }

                if (modelValue != '' && !ctrl.$isEmpty(modelValue)) {
                    return true;
                }
                return false;
            }
        }
    }
});
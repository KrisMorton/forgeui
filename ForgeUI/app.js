﻿/// <reference path="typings/angularjs/angular.d.ts"/>
'use strict';

//Development
var api = "http://forgeapidev.azurewebsites.net/api/";

//Development Local
//var api = "http://localhost:53074/api/";
//Production
//var api = "";
//Staging
//var api = "";

var app = angular.module('Forge', ['ngAnimate','ngAutocomplete', 'ngResource', 'ngRoute', 'AdalAngular', 'ui.bootstrap', 'ui.calendar', 'ngCookies', 'ui.mask', 'underscore', 'angularMoment']);

app.config(['$routeProvider', '$httpProvider', 'adalAuthenticationServiceProvider', function ($routeProvider, $httpProvider, adalAuthenticationServiceProvider) {
    
    $routeProvider
    .when('/', {
        template: '<div></div>',
        controller: 'LoginController',
        resolve: {
        },
        requireADLogin: true
    })
    .when('/Dispatch', {
        templateUrl: 'views/dispatch.html',
        controller: 'DispatchController',
        resolve: {
           
            },
        requireADLogin: true
    })
    .when('/UpsertDispatch', {
        templateUrl: 'views/upsertdispatch.html',
        controller: 'UpsertDispatchController',
        resolve: {
            Dispatch: function () {
                return null;
            }
                
        },
        requireADLogin: true
    })
     .when('/UpsertDispatch/:id', {
         templateUrl: 'views/upsertdispatch.html',
         controller: 'UpsertDispatchController',
         resolve: {
             Dispatch: function ($q, $route, DispatchService) {
                 var deferred = $q.defer();
                 var dispatchID = $route.current.params.id;
                 var promise = DispatchService.Get(dispatchID);
                 promise.then(function (result) {
                     deferred.resolve(result.data);
                 });
                 return deferred.promise;
             },
         },
         requireADLogin: true
     })

    .when('/Home/:username/', {
        templateUrl: 'views/home.html',
        controller: 'HomeController',
        resolve: {
            CurrentUser: function ($q, $route, UserServices) {
                var deferred = $q.defer();
                var username = $route.current.params.username;
                var promise = UserServices.GetForgeUser(username);
                promise.then(function (result) {
                    deferred.resolve(result.data);
                });
                return deferred.promise;
            }
        },
        requireADLogin: true
    })
    .when('/AssignDispatch', {
        templateUrl: 'views/assigndispatch.html',
        controller: 'AssignDispatchController',
        resolve: {

        },
        requireADLogin: true
    })
     .when('/Inspectors', {
         templateUrl: 'views/inspectors.html',
         controller: 'InspectorController',
         resolve: {

         },
         requireADLogin: true
     }),


    //Development
    adalAuthenticationServiceProvider.init(
        {
            tenant: 'bskassocates.onmicrosoft.com',
            clientId: 'b2910015-c5c7-4fce-923c-e1eafcabbe25'
        }, $httpProvider);

  
   

app.constant('ngAuthSettings', {
    apiServiceBaseUri: api
});

}]);

app.config(['$tooltipProvider', function ($tooltipProvider) {
    $tooltipProvider.setTriggers({
        'show': 'hide'
    });
}]);